import random
import string
import sys

class HangMan():

    def __init__(self, language):
        self.guessed = [] # Arvatut kirjaimet
        if language == "1":
            self.word = self.pickFWord()[:-1] # Valitaan satunnaisesti suomenkielinen sana ja poistetaan tyhjämerkki sanan lopusta
        elif language == "2":
            self.word = self.pickEWord()[:-1] # Valitaan satunnaisesti englanninkielinen sana ja poistetaan tyhjämerkki sanan lopusta
        self.tempword = self.word # Piilotettu sana, joka näytetään käyttäjälle
        print(self.word) # Kommentoi tämä pois, jos et halua nähdä oikeaa sanaa alussa
        self.hideWord(self.tempword) # Piilottaa sanan
        self.lives = 10 # Elämät
        self.guessLetter() # Aloitetaan peli

    def pickEWord(self):
        random_word = random.choice(open("Ewords.txt").readlines())
        return random_word

    def pickFWord(self):
        random_word = random.choice(open("Fwords.txt").readlines())
        random_word = random_word.upper()
        return random_word

    def hideWord(self, word):
        self.tempword = word
        for letter in self.word:
            if letter is not "_":
                self.tempword = self.tempword.replace(letter,"_")

    def printLetter(self, correct):
        i = 0
        lista = list(self.tempword) # Muutetaan sana listaksi, jotta kirjain voidaan lisätä piilotettuun sanaan
        for letter in self.word:
            if letter == correct:
                lista[i] = correct
            i+=1
        self.tempword = ''.join(lista)
        self.checkWin(self.tempword)
        self.guessLetter()

    def guessLetter(self):
        print("Arvatut kirjaimet:")
        print(self.guessed)
        print(self.tempword)
        print("Elämiä jäljellä %d kappaletta" % self.lives)
        letter = input("Arvaa kirjain: ").upper()
        print(chr(27) + "[2J")
        if len(letter) < 1:
            print("Epäkelvollinen arvaus. Yritä uudelleen!")
            self.guessLetter()
        elif len(letter) == 1:
            if letter in self.guessed:
                print("Olet jo arvannut tämän kirjaimen! Yritä uudelleen!")
                self.guessLetter()
            elif letter in self.word:
                print("Oikein!")
                self.guessed.append(letter)
                self.printLetter(letter)
            else:
                print("Väärin! Menetit yhden elämän")
                self.guessed.append(letter)
                self.lives-=1
                print("Elämiä jäljellä %d kappaletta" % self.lives)
                if self.lives == 0:
                    self.printLost()
                else:
                    self.guessLetter()
        else:
            if letter == self.word:
                self.printWin()
            else:
                print("Väärin! Menetit yhden elämän")
                self.guessed.append(letter)
                self.lives-=1
                print("Elämiä jäljellä %d kappaletta" % self.lives)
                if self.lives == 0:
                    self.printLost()
                else:
                    self.guessLetter()

        #while letter not in string.ascii_uppercase or len(letter) != 1:
        #    print("Arvauksesi on epäkelvollinen. Yritä uudestaan")
        #    letter = input("Arvaa kirjain: ")
        #    letter = letter.upper()
        #self.checkLetter(letter)
    '''
    def checkLetter(self, letter):
        print(chr(27) + "[2J") # Printataan tyhjää komentoriville
        print("Elämiä jäljellä %d kappaletta" % self.lives)
    '''


    def checkWin(self,letter):
        if self.tempword == self.word:
            self.printWin()

    def printWin(self):
        print(chr(27) + "[2J")
        print("Arvasit sanan oikein!")
        print("Elämiä jäi jäljelle %d kappaletta" % self.lives)
        print("Sana oli: %s" % self.word)
        sys.exit()

    def printLost(self):
        print(chr(27) + "[2J")
        print("Hävisit pelin!")
        print("Sana oli: %s" % self.word)
        sys.exit()


if __name__ == '__main__':
    print("                                              ")
    print("                                              ")
    print("                                              ")
    print("                                              ")
    print("                                              ")
    print("                                              ")
    print("                                              ")
    print("                                              ")
    print("                                              ")
    print("                                              ")
    print("                                              ")
    print("                                              ")
    print("                                              ")
    print("-------------------------TERVETULOA PELAAMAAN HIRSIPUUTA------------------------")
    print("                                              ")
    print("                                              ")
    print("                                              ")
    print("                                              ")
    print("                                              ")
    print("                                              ")
    print("                                              ")
    print("                                              ")
    print("                                              ")
    print("                                              ")
    print("                                              ")
    print("                                              ")

    language = input("Jos haluat pelata suomenkielisillä sanoilla paina '1', jos englanninkielisillä sanoilla paina '2': ")
    while language != '1' and language != '2':
        print("Epäkelvollinen valinta. Yritä uudestaan.")
        language = input("Jos haluat pelata suomenkielisillä sanoilla paina '1' ja jos englanninkielisillä sanoilla paina '2': ")
    print(chr(27) + "[2J") # Printataan tyhjää komentoriville
    HangMan(language)
