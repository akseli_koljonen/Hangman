import sys
import os
import random
import string
from hangman import HangMan
from PyQt5.QtWidgets import QWidget, QApplication, QMessageBox,QFrame,QPushButton,QHBoxLayout, QVBoxLayout, QLabel, QMainWindow,QAction,qApp,QFileDialog
from PyQt5.QtGui import QPainter, QColor, QFont, QKeyEvent,QPen,QIcon,QImage,QPixmap
from PyQt5.QtCore import Qt, QTimer, QDir

class MainWindow():
    def __init__(self,language):
        #super().__init__()
        self.guessed = []
        if language == "1":
            self.word = self.pickFWord()[:-1] # Valitaan satunnaisesti suomenkielinen sana ja poistetaan tyhjämerkki sanan lopusta
        elif language == "2":
            self.word = self.pickEWord()[:-1] # Valitaan satunnaisesti englanninkielinen sana ja poistetaan tyhjämerkki sanan lopusta
        self.tempword = self.word
        self.hangman = HangMan()
        self.hangman.hideWord(self.tempword)
        self.lives = 10 # Elämät
        #self.chunkio = ChunkIO()
        self.hangman.guessLetter()


    def pickEWord(self):
        random_word = random.choice(open("Ewords.txt").readlines())
        return random_word

    def pickFWord(self):
        random_word = random.choice(open("Fwords.txt").readlines())
        random_word = random_word.upper()
        return random_word


if __name__ == '__main__':
    language = input("Jos haluat pelata suomenkielisillä sanoilla paina '1', jos englanninkielisillä sanoilla paina '2': ")
    while language != '1' and language != '2':
        print("Epäkelvollinen valinta. Yritä uudestaan.")
        language = input("Jos haluat pelata suomenkielisillä sanoilla paina '1' ja jos englanninkielisillä sanoilla paina '2': ")
    MainWindow(language)
